<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.4.0.min.js"
            integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
            crossorigin="anonymous">
        </script>
        <!-- Styles -->
        
        <style>
            body {
                overflow: scroll;
            }
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                /* display: none; <- Crashes Chrome on hover */
                -webkit-appearance: none;
                margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
            }

            input[type=number] {
                -moz-appearance:textfield; /* Firefox */
            }

            .detalle {
                border: 1px solid lightgray;
                padding: 10px;
                border-radius: 5px;
            }
        </style>
    </head>
    <body>
        <a href="/" style="font-size: 20px">Volver a ingresos</a>
        <div class="row">
            <div class="col-6" style="padding: 10px; padding-left: 30px; border: 1px solid lightgray">
                    <h2>Ingreso: </h2>
                    <label for="">Id: </label><p id="id_ingreso">{{$ingreso->id}}</p>
                    <p>Factura: {{$ingreso->numero_factura}}</p> 
                    <p>Tercero: {{$ingreso->tercero->nombres}}</p> 
                    <p>Banco: {{$ingreso->banco->nombre}}</p>
                    <p>Valor: {{$ingreso->valor}}</p>
                    <p>Fecha: {{$ingreso->fecha_recaudo}}</p>
            </div>
            <div class="col-6" style="padding: 10px; border: 1px solid lightgray">
                <h2>Añadir detalle</h2>
            <form action="{{route('ingresar_detalle')}}" id="detalle_form">
                    <label for="concepto">Concepto: </label>
                    <select name="concepto" id="fk_id_concepto" required>
                        @foreach ($conceptos as $concepto)
                            <option value="{{$concepto->id}}">{{$concepto->nombre}}</option>
                        @endforeach
                    </select>
                    <label for="valor">Valor: </label>
                    <input type="text" name="valor" id="valor" required>
                    <button type="submit">Añadir detalle</button>
                </form>
            </div>
        </div>
        <h3>Detalles</h3>
        <div id="detalles">
        @foreach ($detalles as $detalle)
            <div class="detalle">
                <h4>Detalle {{$detalle->id}}</h4>
                <p>Valor: {{$detalle->valor}}</p>
                <p>Concepto: {{$detalle->concepto->nombre}}</p>
            </div>
        @endforeach
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#detalle_form").submit(function(event) {
                    event.preventDefault();
                    var $form = $(this);
                    var url = $form.attr('action');
                    console.log(url);
                    data = {
                        valor: $("#valor").val(),
                        fk_id_ingreso: $("#id_ingreso").html(),
                        fk_id_concepto: $("#fk_id_concepto").val()
                    };

                    console.log(data);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: data,
                        type: 'json',
                        success: function(response){
                            id = response.data.id;
                            valor = response.data.valor;
                            concepto = response.data.concepto.nombre;

                            console.log(response);
                            alert("Detalle agregado con éxito");
                            $element = 
                            `<div class="detalle">
                                <h4>Detalle ${id}</h4>
                                <p>Valor: ${valor}</p>
                                <p>Concepto: ${concepto}</p>
                            </div>`;

                            $("#detalles").prepend($element);
                            window.location.reload();

                        }
                    });                               
                });
            });
        </script>
    </body>
</html>
