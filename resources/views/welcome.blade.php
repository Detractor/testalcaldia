<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.4.0.min.js"
            integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
            crossorigin="anonymous">
        </script>
        <!-- Styles -->
        
        <style>
            body {
                overflow: scroll;
            }
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                /* display: none; <- Crashes Chrome on hover */
                -webkit-appearance: none;
                margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
            }

            input[type=number] {
                -moz-appearance:textfield; /* Firefox */
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div style="margin: 0 auto; margin-top: 40px; max-width: 1000; width: 80%; border: 1px solid lightgray; border-radius: 5px; padding: 40px; padding-bottom: 60px">
                <h2>Haz un nuevo ingreso: </h2>
            <form id="ingreso" action="{{route("nuevo_ingreso")}}">
                    <div class="form-group">
                        <label for="numero_factura">Número de factura: </label>
                        <input type="number" class="form-control" id="numero_factura" placeholder="Ingresa el número de la factura" required>
                    </div>
                    <div class="form-group">
                        <label for="banco">Banco: </label>
                        <select class="form-control" id="banco" required>
                            <option value="">[Seleciona un banco]</option>
                            @foreach ($bancos as $banco)
                                <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tercero">Tercero: </label>
                        <select class="form-control" id="tercero" required>
                            <option value="">[Seleciona un tercero]</option>
                            @foreach ($terceros as $tercero)
                                <option value="{{$tercero->id}}">{{"$tercero->nombres $tercero->apellidos"}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="valor">Valor: </label>
                        <input type="number" class="form-control" id="valor" placeholder="Ingresa el valor del recaudo" required>
                    </div>
                    <div class="form-group">
                        <label for="fecha_recaudo">Fecha del recaudo: </label>
                        <input type="date" class="form-control" id="fecha_recaudo" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Guardar recaudo</button>
                </form>

                <table class="table" style="margin-top: 30px">
                <h2 style="margin-top: 50px">Todos los ingresos</h2>
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col"># Factura</th>
                        <th scope="col">Tercero</th>
                        <th scope="col">Banco</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Fecha del recaudo</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody id="ingreso_body">
                        @foreach ($ingresos as $ingreso)
                        <tr>
                            <td>{{$ingreso->id}}</td>
                            <td>{{$ingreso->numero_factura}}</td>
                            <td>{{$ingreso->tercero->nombres}}</td>
                            <td>{{$ingreso->banco->nombre}}</td>
                            <td>{{$ingreso->valor}}</td>
                            <td>{{$ingreso->fecha_recaudo}}</td>
                            <td>
                                <a href="{{route("ver_ingreso", ['id' => $ingreso->id])}}" class="small btn btn-success btn-sm">Ver</a>
                            </td>
                        </tr>                        
                        @endforeach     
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#ingreso").submit(function(event) {
                    event.preventDefault();
                    var $form = $(this);
                    var url = $form.attr('action');
                    console.log(url);
                    data = {
                        fk_id_tercero: $("#tercero").val(),
                        fk_id_banco: $("#banco").val(),
                        numero_factura: $("#numero_factura").val(),
                        valor: $("#valor").val(),
                        fecha_recaudo: $("#fecha_recaudo").val(),
                    };
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: data,
                        type: 'json',
                        beforeSend: function(){

                        },
                        success: function(response){
                            id = response.data.id;
                            banco = response.data.banco.nombre;
                            tercero = response.data.tercero.nombres;
                            valor = response.data.valor;
                            fecha = response.data.fecha_recaudo;
                            factura = response.data.numero_factura;
                            $row = 
                            `<tr>
                                <td>${id}</td>
                                <td>${factura}</td>
                                <td>${tercero}</td>
                                <td>${banco}</td>
                                <td>${valor}</td>
                                <td>${fecha}</td>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm">Ver</a>
                                </td>                            
                            </tr>`
                            var mensaje = `Se ha creado un ingreso exitosamente con la factura ${factura} \nPara el usuario ${tercero}, en el banco ${banco} \n\nPor un valor de ${valor}`;
                            $("#ingreso_body").prepend($row);
                            alert(mensaje);
                            window.location.reload();
                        }
                    });                               
                });
            });
        </script> 
    </body>
</html>
