<?php
use App\Models\Banco;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $bancos = Banco::all();
    $view_data = [
        'bancos' => $bancos,
    ];

    return view('welcome', $view_data);
});

Route::get('/', 'IngresoController@index')->name('Ingresos');
Route::post('/ingresos', 'IngresoController@store')->name("nuevo_ingreso");
Route::get('/ingresos/{id}', 'IngresoController@show')->name("ver_ingreso");
Route::post('/detalles', 'DetalleController@store')->name("ingresar_detalle");
