<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tercero;
use App\Models\Banco;
use App\Models\Ingreso;
use App\Models\IngresoDetalle;
use App\Models\Concepto;

class IngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos = Banco::all();
        $terceros = Tercero::all();
        $ingresos = Ingreso::with(['banco', 'tercero'])->orderBy('id', 'DESC')->get();

        $view_data = [
            'bancos' => $bancos,
            'terceros' => $terceros,
            'ingresos' => $ingresos
        ];
    
        return view('welcome', $view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'fk_id_tercero',
            'fk_id_banco',
            'numero_factura',
            'valor',
            'fecha_recaudo',
        ];

        $data = $request->only($fields);

        $ingreso = Ingreso::create($data);
        $ingreso->tercero;
        $ingreso->banco;
        return response()->json(['data' => $ingreso]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingreso = Ingreso::find($id);
        $detalles = IngresoDetalle::where('fk_id_ingreso', $id)->orderBy('id', 'DESC')->get();
        $conceptos = Concepto::all();

        $view_data = [
            'ingreso' => $ingreso,
            'detalles' => $detalles,
            'conceptos' => $conceptos
        ];

        return view('ingresos', $view_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
