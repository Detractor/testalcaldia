<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IngresoDetalle;

class DetalleController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'valor',
            'fk_id_ingreso',
            'fk_id_concepto'
        ];

        $data = $request->only($fields);

        $detalle = IngresoDetalle::create($data);
        $detalle->concepto;

        return response()->json(['data' => $detalle]);
    }

}
