<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banco extends Model
{
    use SoftDeletes;

    protected $table = 'tr_banco';
    protected $fillable = [
        'nombre',
        'descripcion'
    ];

    public function ingresos()
    {
        return $this->hasMany(Ingreso::class, 'fk_id_banco');
    }

    public function getNombreAttribute($value)
    {
        return "Banco " . ucwords($value);
    }

}
