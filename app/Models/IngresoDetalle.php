<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IngresoDetalle extends Model
{
    use SoftDeletes;
    
    protected $table = 'td_ingreso_detalle';
    protected $fillable = [
        'valor',
        'fk_id_ingreso',
        'fk_id_concepto'
    ];

    public function ingreso()
    {
        return $this->belongsTo(Ingreso::class, 'fk_id_ingreso');
    }
    
    public function concepto()
    {
        return $this->belongsTo(Concepto::class, 'fk_id_concepto');
    }
    
    
}
