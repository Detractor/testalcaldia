<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table = 'tm_ingreso';

    protected $fillable = [
        'fk_id_tercero',
        'fk_id_banco',
        'numero_factura',
        'valor',
        'fecha_recaudo',
    ];

    protected $casts = [
        'valor' => 'integer',
        'fecha_recaudo' => 'date'
    ];

    public function banco()
    {
        return $this->belongsTo(Banco::class, 'fk_id_banco');
    }

    public function tercero()
    {
        return $this->belongsTo(Tercero::class, 'fk_id_tercero');
    }

    public function detalles()
    {
        return $this->hasMany(IngresoDetalle::class, 'fk_id_ingreso');
    }


}
