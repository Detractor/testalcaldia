<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tercero extends Model
{
    use SoftDeletes;
    protected $table = 'tr_tercero';

    protected $fillable = [
        'nombres',
        'apellidos',
    ];

    public function ingresos()
    {
        return $this->hasMany(Ingreso::class, 'fk_id_tercero');
    }

    
}
