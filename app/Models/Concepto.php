<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Concepto extends Model
{
    use SoftDeletes;
    
    protected $table = 'tr_concepto';
    protected $fillable = [
        'nombre',
        'descripcion'
    ];

    public function detalles()
    {
        return $this->hasMany(IngresoDetalle::class, 'fk_id_concepto');
    }
}
