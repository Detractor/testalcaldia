Proyecto creato para la convocatoria de programador de Laravel para la Alcaldía Municipal de Ibagué

- Se crearon dos vistas.
Para mayor conveniencia, se usó la ruta home (/) para la inserción de ingresos...
Y para ver e ingresar detalle al ingreso seleccionado se usó la ruta (/ingresos/{id});
Donde se puede añadir el detalle de un ingreso, el cual se refresca usando AJAX.

- En la vista principal se pueden hacer ingresos, proporcionando la información solicitada, también se refresca usando AJAX.

- Se crearon unos factories que llenan las tablas Banco, Tercero y Concepto de registros falsos, por lo que se recomienda correr el comando

php artisan migrate:fresh --seed 

para evitar llenar estos registros a mano al momento de la prueba