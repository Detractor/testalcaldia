<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Banco::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->paragraph(1)
    ];
});
