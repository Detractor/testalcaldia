<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Concepto::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->paragraph(1)
    ];
});
