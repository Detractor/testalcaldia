<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tercero::class, function (Faker $faker) {
    return [
        'nombres' => $faker->firstName,
        'apellidos' => $faker->lastName,
    ];
});
