<?php

use Illuminate\Database\Seeder;
use App\Models\Banco;
use App\Models\Concepto;
use App\Models\Tercero;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(Banco::class, 20)->create();
        factory(Concepto::class, 100)->create();
        factory(Tercero::class, 15)->create();
    }
}
