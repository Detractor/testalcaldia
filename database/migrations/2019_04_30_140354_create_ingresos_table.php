<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_ingreso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_factura');
            $table->decimal('valor');
            $table->datetime('fecha_recaudo');
            $table->unsignedInteger('fk_id_tercero');
            $table->unsignedInteger('fk_id_banco');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_tercero')->references('id')->on('tr_tercero');
            $table->foreign('fk_id_banco')->references('id')->on('tr_banco');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_ingreso');
    }
}
