<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_ingreso_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_ingreso');
            $table->unsignedInteger('fk_id_concepto');
            $table->decimal('valor');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_ingreso')->references('id')->on('tm_ingreso');
            $table->foreign('fk_id_concepto')->references('id')->on('tr_concepto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('td_ingreso_detalle');
    }
}
